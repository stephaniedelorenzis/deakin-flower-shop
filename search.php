<?php include("include/header.php"); ?>
   	    <div class="left_content">
          <input type="text" name="search"><button type="button" onclick="search()">
            <div class="clear"></div>
        </div><!--end of left content-->

        <div class="right_content">
      	   <div class="languages_box">
              <span class="red">Languages:</span>
              <a href="#"><img src="images/au.gif" alt="" title="" border="0" height="12px" width="15px"/></a>
          </div>
          <div class="currency">
            <span class="red">Currency: </span>
            <a href="#" class="selected">AUD</a>
          </div>
          <div class="cart">
              <div class="title"><span class="title_icon"><img src="images/cart.gif" alt="" title="" /></span>My cart</div>
              <div class="home_cart_content">
              3 x items | <span class="red">TOTAL: 100$</span>
              </div>
              <a href="cart.html" class="view_cart">view cart</a>
          </div>

           <div class="title"><span class="title_icon"><img src="images/bullet3.gif" alt="" title="" /></span>About Our Shop</div>
           <div class="about">
           <p>
           <img src="images/about.gif" alt="" title="" class="right" />
           Flowershop has quickly become renowned as one of Geelong's most prestigious and luxurious retail flower stores, and this has been successfully translated to our online flower shop. The same service, quality and range we provide to our retail shoppers is also extended to our online community. <!-- reference: Flower Temple, availalbe at <http://www.flowertemple.com.au/aboutflowertemple.aspx>, accessed 09/07/2013)-->
           </p>

           </div>

        <div class="right_box">
         	<div class="title"><span class="title_icon"><img src="images/bullet4.gif" alt="" title="" /></span>Promotions</div>
                <div class="new_prod_box">
                    <a href="details.html">product name</a>
                    <div class="new_prod_bg">
                    <span class="new_icon"><img src="images/promo_icon.gif" alt="" title="" /></span>
                    <a href="details.html"><img src="images/thumb1.gif" alt="" title="" class="thumb" border="0" /></a>
                    </div>
                </div>

                <div class="new_prod_box">
                    <a href="details.html">product name</a>
                    <div class="new_prod_bg">
                    <span class="new_icon"><img src="images/promo_icon.gif" alt="" title="" /></span>
                    <a href="details.html"><img src="images/thumb2.gif" alt="" title="" class="thumb" border="0" /></a>
                    </div>
                </div>
         </div>

       <div class="right_box">

       	<div class="title"><span class="title_icon"><img src="images/bullet5.gif" alt="" title="" /></span>Categories</div>

          <ul class="list">
          <li><a href="#">accesories</a></li>
          <li><a href="#">flower gifts</a></li>
          <li><a href="#">specials</a></li>
          <li><a href="#">hollidays gifts</a></li>
          <li><a href="#">accesories</a></li>
          <li><a href="#">flower gifts</a></li>
          <li><a href="#">specials</a></li>
          <li><a href="#">hollidays gifts</a></li>
          <li><a href="#">accesories</a></li>
          <li><a href="#">flower gifts</a></li>
          <li><a href="#">specials</a></li>
          </ul>
       </div>
    </div><!--end of right content-->
<?php include("include/footer.php"); ?>
