<?xml version = "1.0" encoding="UTF-8"?>
<xsl:stylesheet version = "1.0" xmlns:xsl = "http://www.w3.org/1999/XSL/Transform">
   <xsl:output method = "html" omit-xml-declaration = "no"
      doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
      doctype-public = "-//W3C//DTD XHTML 1.0 Strict//EN" />

	<xsl:param name="flowerid" />
<xsl:template match = "/CATALOG">
		<xsl:for-each select="PLANT[@id=$flowerid]">
			<div class="crumb_nav">
				<a href="./ass1.php">Home</a> &gt;&gt; <a href="./category.php">Flowers</a> &gt;&gt; <xsl:value-of select="NAME/COMMON"/>
			</div>
			<div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span><xsl:value-of select="NAME/COMMON"/></div>

			<div class="feat_prod_box_details">

			   <div class="prod_img">
				   <img title="" class="thumb" border="0">
						<xsl:attribute name="src">
							./images/flower_photos/<xsl:value-of select="PHOTO"/>
						</xsl:attribute>
						<xsl:attribute name="alt">
							<xsl:value-of select="NAME/COMMON"/>
						</xsl:attribute>
					</img>
				 <br /><br />
					<a rel="lightbox">
						<xsl:attribute name="href">
							./images/flower_photos/<xsl:value-of select="PHOTO"/>
						</xsl:attribute>
						<img src="images/zoom.gif" alt="" title="" border="0" />
					</a>
				</div>

				 <div class="prod_det_box">
					<div class="box_top"></div>
					 <div class="box_center">
						<div class="prod_title">Details</div>
						<p class="details">
							<xsl:value-of select="DETAILS"/>
						</p>
						<div class="price"><strong>PRICE:</strong> <span class="red"> <xsl:value-of select="PRICE"/></span></div>
						<div class="price"><strong>COLORS:</strong>
							<xsl:for-each select="COLOURSAVAILABLE/COLOUR">
								<div class="colors">
									<xsl:attribute name="style">
										background-color:<xsl:value-of select="text()"/>
									</xsl:attribute>
								</div>
							</xsl:for-each>
						</div>
					   <a href="#" class="more"><img src="images/order_now.gif" alt="" title="" border="0" /></a>
					   <div class="clear"></div>
					 </div>

					<div class="box_bottom"></div>
				 </div>
				<div class="clear"></div>
			</div>
		</xsl:for-each>
 </xsl:template>
 </xsl:stylesheet>
