<?xml version = "1.0" encoding="UTF-8"?>
<xsl:stylesheet version = "1.0" xmlns:xsl = "http://www.w3.org/1999/XSL/Transform">
   <xsl:output method = "html" omit-xml-declaration = "no"
      doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
      doctype-public = "-//W3C//DTD XHTML 1.0 Strict//EN" />

   <xsl:template match = "/CATALOG">
      <html>
        <body>
          <xsl:for-each select = "PLANT">
            <div class="new_prod_box">
				<!-- Name of the product with link to product details page -->
                <a>
                  <xsl:attribute name="href">
                    details.php?id=<xsl:value-of select="ID"/>
                  </xsl:attribute>
                  <xsl:value-of select="NAME/COMMON"/>
                </a>
                <div class="new_prod_bg">
					<!-- link to the product details page -->
					<a>
					  <xsl:attribute name="href">details.php?id=<xsl:value-of select="@id"/></xsl:attribute>
					  <!-- Add image of the product -->
					  <img title="" class="thumb" border="0">
						<xsl:attribute name="src">
							./images/flower_photos/<xsl:value-of select="PHOTO"/>
						</xsl:attribute>
						<xsl:attribute name="alt">
							<xsl:value-of select="NAME/COMMON"/>
						</xsl:attribute>
					  </img>
					</a>
                </div>
            </div>
          </xsl:for-each>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>
