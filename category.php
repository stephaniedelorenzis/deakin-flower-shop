<?php
  include("include/header.php");
?>
 	   <div class="left_content">
  	    <div class="crumb_nav">
          <a href="index.html">home</a> &gt;&gt; Flowers
        </div>
        <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Flowers</div>

        <!-- div for xml content -->
        <div id="new_products" class="new_products">
        </div>

        <div class="clear"></div>
    </div><!--end of left content-->
<script type="text/javascript">
	window.onload = function (){
		displayResult("xml/catalog.xml", "xml/catalog.xsl", "new_products");
	};
 
</script>
<?php
  include("include/rightContent.php");
  include("include/footer.php");
?>
