<?php
	//INCLUDE THE HEADER
  include("include/header.php");
  //STATES OF AUS FOR BILLER INFO
  $states = array("ACT","NSW","NT","QLD","SA","TAS","VIC","WA");
 ?>
       	<div class="left_content">
            <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Checkout</div>

        	<div class="feat_prod_box_details">

				<table class="cart_table">
					<tr class="cart_title">
						<td>Item pic</td>
						<td>Product name</td>
						<td>Unit price</td>
						<td>Qty</td>
						<td>Total</td>
					</tr>

					<tr>
						<td><a href="details.html"><img src="images/cart_thumb.gif" alt="" title="" border="0" class="cart_thumb" /></a></td>
						<td>Gift flowers</td>
						<td>100$</td>
						<td>1</td>
						<td>100$</td>
					</tr>
					<tr>
						<td><a href="details.html"><img src="images/cart_thumb.gif" alt="" title="" border="0" class="cart_thumb" /></a></td>
						<td>Gift flowers</td>
						<td>100$</td>
						<td>1</td>
						<td>100$</td>
					</tr>
					<tr>
						<td><a href="details.html"><img src="images/cart_thumb.gif" alt="" title="" border="0" class="cart_thumb" /></a></td>
						<td>Gift flowers</td>
						<td>100$</td>
						<td>1</td>
						<td>100$</td>
					</tr>

					<tr>
						<td colspan="4" class="cart_total"><span class="red">TOTAL SHIPPING:</span></td>
						<td> 250$</td>
					</tr>

					<tr>
						<td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
						<td> 325$</td>
					</tr>
				</table>

				
				<!-- TABLE FOR ORDERING THE FLOWERS -->
				<br/>
				<h3>Order Form</h3>
				<div class="order-form">
					<form>
						<fieldset id="contactDetails">
							<div class="field-title">Contact Details</div>
							<div class="form-row">
								<label class="form-label" for="fname">First Name</label>
								<input type="text" name="fname" autocomplete="name" required/>
								<div class="alert alert-invalid"></div>
							</div>
							<div class="form-row">
								<label class="form-label" for="lname">Last Name</label>
								<input type="text" name="lname" autocomplete="name" required/>
								<div class="alert alert-invalid"></div>
							</div>
							<div class="form-row">
								<label class="form-label" for="email">Email</label>
								<input type="text" name="email" autocomplete="email" required/>
								<div class="alert alert-invalid"></div>
							</div>
							<div class="form-row">
								<label class="form-label" for="phone">Phone Number</label>
								<input type="text" name="phone" autocomplete="tel" required/>
								<div class="alert alert-invalid"></div>
							</div>							
						</fieldset>
						
						<fieldset>
							<div class="field-title">Delivery Details</div>
							<div class="form-row">
								<label class="form-label" for="streetNum">Street Number</label>
								<input type="text" name="streetNum" required />
								<label class="form-label" for="streetName">Street Name</label>
								<input type="text" name="streetName" required />
								<div class="alert alert-invalid"></div>
							</div>
							<div class="form-row">
								<label class="form-label" for="suburb">Suburb</label>
								<input type="text" name="suburb" autocomplete="suburb" required />
								<div class="alert alert-invalid"></div>
							</div>	
							<div class="form-row">
								<label class="form-label" for="postcode">PostCode</label>
								<input type="text" name="postcode" autocomplete="postcode" required />
								<div class="alert alert-invalid"></div>
							</div>
							<div class="form-row">
								<label class="form-label" for="state">State</label>
								<!-- THE STATES OF AUS FROM LIST ABOVE -->
								<select id="state" name="state" class="stateSelect" required>
									<?php
										$options = "<option value='-1' disabled selected>State</option>";
										for($i = 0; $i < count($states); $i++)
										{	
											$val = $states[$i];
											$options .= "<option value='$val' >$val</option>";
										}
										
										echo $options;							
									?>
								</select>			
								<div class="alert alert-invalid"></div>
							</div>							
						</fieldset>
						
						<fieldset>
							<div class="field-title">Payment Details</div>
							<div class="form-row">
								<label class="form-label" for="paymentType">Card Type</label>
								<select name="cardType">
									<option value="-1" disabled selected>Choose card type</option>
									<option value="mastercard">MasterCard</option>
									<option value="visa">Visa</option>
									<option value="amex">AMEX</option>
								</select>								
								<div class="alert alert-invalid"></div>
							</div>
							<div class="form-row">
								<label class="form-label" for="cardNumber">Card Number</label>
								<input type="text" name="cardNumber" autocomplete="cardnumber" required/>
								<div class="alert alert-invalid"></div>
								<label class="form-label" for="cvc">CVC / CVV</label>
								<input type="text" name="cvc" required/>
								<div class="alert alert-invalid"></div>
							</div>	
							<div class="form-row">
								<label class="form-label" for="cardName">Card Name</label>
								<input type="text" name="cardName" autocomplete="cardname" required/>
								<div class="alert alert-invalid"></div>
							</div>	
							<div class="form-row">
								<label class="form-label" for="expiryMonth">Expiry</label>
								<select id="expMonth" name="expMonth">
								<option value="-1" disabled selected>MM</option>
								<?php
									$months = "";
									for($i = 1; $i <= 12; $i++)
									{
										//if month is less than 10 add preceding zero
										$str = $i;
										if($str < 10)
										{
											$str = "0$str";
										}
										$months .= "<option value='$i'>$str</option>";
									}
									echo $months;
								?>
								</select>
								/
								<select id="expYear" name="expYear">
									<option value="-1" disabled selected>YYYY</option>
									<?php
										$years = "";
										//find this years date and count forward ten years.
										//credit cards won't be valid longer then ten years.
										//self maintaining
										$thisYear = date('Y');
										for($i = $thisYear; $i <= $thisYear+10; $i++)
										{
											$years .= "<option value='$i'>$i</option>";
										}
										echo $years;
									?>
								</select>
								<div class="alert alert-invalid"></div>
							</div>	
						</fieldset>
					</form>
				</div>
				<!-- <table class="checkout_table">
					<!-- CONTACT INFORMATION 
			
					<tr class="header_row">
						<th colspan="2">Contact Details</th>
					</tr>
					<tr>
						<th>First Name*</th>
						<td><input type="text" placeholder="First Name" name="firstname"></td>
					</tr>
					<tr>
						<th>Last Name*</th>
						<td><input type="text" placeholder="Last Name" name="lastname"></td>
					</tr>
					<tr>
						<th>Email Address*</th>
						<td><input type="email" placeholder="Email" name="email"></td>
					</tr>
					<tr>
						<th>Phone*</th>
						<td><input type="tel" placeholder="Phone" name="phone"></td>
					</tr>
					<!-- DELIVERY DETAILAS 
					<tr class="header_row">
						<th colspan="2">Delivery details</th>
					</tr>
					<tr>
						<th>House number*</th>
						<td><input type="text" placeholder="House Number" name="housenum"></td>
					</tr>
					<tr>
						<th>Street name*</th>
						<td><input type="text" placeholder="Street Name" name="street"></td>
					</tr>
					<tr>
						<th>Suburb*</th>
						<td><input type="text" placeholder="Suburb" name="suburb"></td>
					</tr>
					<tr>
						<th>Post Code*</th>
						<td><input type="text" placeholder="Post Code" name="postcode"></td>
					</tr>
					<tr>
						<th>State*</th>
						<td>
							<!-- THE STATES OF AUS FROM LIST ABOVE 
							<select id="state" name="state" class="stateSelect">
								<?php
									// $options = "<option value='null'>State</option>";
									// for($i = 0; $i < count($states); $i++)
									// {	
										// $val = $states[$i];
										// $options .= "<option value='$val'>$val</option>";
									// }
									
									// echo $options;							
								 ?>
							</select>			
						</td>
					</tr>
					<!-- PAYMENT METHOD 
					<tr class="header_row">
						<th colspan="2">Payment Method</th>
					</tr>
					<tr>
						<th>Payment Type*</th>
						<td class="paymentCell">
							<input id="paymentCredit" type="radio" name="paymentType" value="credit" checked>
							<img class="creditCardImg" alt="Credit Card &nbsp;" src="images/creditCards.png">
							&nbsp;&nbsp;
							<input id="paymentPaypal" type="radio" name="paymentType" value="paypal" onclick="paypal()">
							<img class="creditCardImg" alt="Paypal" src="images/paypal.png">
						</td>
					</tr>
					<tr>
						<th>Card Number*</th>
						<td><input type="text" placeholder="Card Number" name="cardnumber"></td>
					</tr>
					<tr>
						<th>Expiry Date*</th>
						<td class="expiryCell">
							<!--expiry month, year, code 
							<select id="expMonth" name="expMonth">
								<option value="null">MM</option>
								<?php
									// $months = "";
									// for($i = 1; $i <= 12; $i++)
									// {
										//if month is less than 10 add preceding zero
										// $str = $i;
										// if($str < 10)
										// {
											// $str = "0$str";
										// }
										// $months .= "<option value='$i'>$str</option>";
									// }
									// echo $months;
								?>
							</select>
							/
							<select id="expYear" name="expYear">
								<option value="null">YYYY</option>
								<?php
									// $years = "";
									// find this years date and count forward ten years.
									// credit cards won't be valid longer then ten years.
									// self maintaining
									// $thisYear = date('Y');
									// for($i = $thisYear; $i <= $thisYear+10; $i++)
									// {
										// $years .= "<option value='$i'>$i</option>";
									// }
									// echo $years;
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th>Cardholder Name*</th>
						<td><input type="text" placeholder="Cardholder Name" name="cardholdername"></td>
					</tr>
					<tr>
						<th>CVC / CVV*</th>
						<td><input type="text" name="code" id="code" placeholder="Code"></td>
					</tr>
					<tr>
						<td colspan='2'><div class='pull-right'><em>*All fields required</em></div></td>
					</tr>
				</table> -->
				<a href="#" class="checkout">Continue &gt;</a>
			</div>






        <div class="clear"></div>
        </div><!--end of left content-->
		<script type="text/javascript">
			$(function(){ 
			$(".alert").hide();
			
			//validate the order form
				$(".order-form input, .order-form select").blur(function(){
					var parent = $(this).parent();
										//make ajax function 
					//THIS IS DEFINITELY THE WRONG WAY TO DO THIS. VALIDATION ON THE FLY SHOULD BE DONE VIA 
					//JQUERY
					$.ajax({
						url: "include/validate.php",
						data: {name : $(this).attr("name"), value: $(this).val()}, 
						method: "POST",			
					}).done(function(res){
						var validation = JSON.parse(res);
						
						if(!validation.valid){							
							parent.addClass("invalid");
							parent.find(".alert").text(parent.text()+validation.msg).show();
							console.log(parent);
						} else {
							parent.removeClass("invalid");
							parent.find(".alert").hide().text("");
						}
					});					
				});
			});
			
			$("[name='cardNumber']").change(function(){
				var val = $(this).val();
				//when there are 4 digits together add a space after them
				val.replace(/([0-9]{4})/g, "$1 ");
			});
			
		</script>

<?php
  include("include/rightContent.php");
  include("include/footer.php");
 ?>
