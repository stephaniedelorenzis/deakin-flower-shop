<?php
	$name = $_POST["name"];
	$val = $_POST["value"];
	
	$valid = false;
	$msg = "";
	
	switch($name) {
		case "fname":
		case "lname":
		case "cardName":
		case "suburb":
		case "streetName":
			$valid = validateName($val);
			$msg = "Name is not valid";
			break;
		case "phone":
			$valid = validatePhoneNumber($val);
			$msg = "Phone number is not valid";
			break;
		case "cardNumber":
			$valid =  validateCardNumber($val);
			$msg = "Card number is no valid";
			break;
		case "cvc":
			$valid = validateCvc($val);
			$msg = "CVC is not valid";
			break;
		case "postcode":
			$valid = validatePostcode($val);
			$msg = "PostCode is not valid";
			break;
		case "email":
			$valid = validateEmail($val);
			$msg = "Email is not valid";
			break;
		case "expMonth":
		case "expYear":
		case "cardType":
		case "state":
			$valid = ($val != -1);
			$msg = "Please select a choice";
			break;		
	}
	

	
	//return new object with valid status and a message to put in the alert box
	echo json_encode(array("valid" => $valid, "msg" => $msg));
	

	/*
	* Validates the name
	*/
	function validateName($name) {

		//letter, spaces, hyphens and apostrophes only
		return preg_match("/^[a-z\- \']{1,}$/i", $name) == 1;
	}

	/*
	* Validate a phone number
	*/
	function validatePhoneNumber($number) {
		//can start with a + (optional)
		//numbers and spaces
		//8 - 11 chars
		return preg_match("/^\+?[0-9 ]{8,11}$/", $number) == 1;
	}

	/*
	* Validate the email address
	*/
	function validateEmail($email) {
		//check for a-z any case
		//dots and hyphens and underscores in first part of email
		//one @ separating the parts
		//letters and dots in the second half of the email
		//return preg_match( "/^[a-z\.\-_]*@[a-z\.]{1,}/i", email);
		return filter_var($email, FILTER_VALIDATE_EMAIL) && count(trim($email)) > 0 ;
	}

	/*
	* Validate the card number
	*/
	function validateCardNumber($number) {
		//4 sets of 4 numbers with an optional space separating them
		return preg_match("/^([0-9]{4} ?){4}$/", $number) == 1;
	}

	/*
	* Validates the post code to 4 digits
	*/
	function validatePostcode($postcode) {
		//set of 4 numbers
		return preg_match("/^[0-9]{4}$/", $postcode) == 1;
	}

	/*
	* Validate the CVC
	*/
	function validateCvc($cvc) {
		//set of three digits
		return preg_match("/^[0-9]{3}$/", $cvc) == 1;
	}
?>