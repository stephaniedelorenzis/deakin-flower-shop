<!-- sdelor Footer info moved to footer file 11/9/2016 -->

<div class="clear"></div>
</div><!--end of center content-->


<div class="footer">
<?php $_SERVER['PHP_SELF']; ?>
<?php basename($_SERVER['PHP_SELF']); ?>
  <!-- sdelor disclaimer added  12/9/2016-->
  <div class="disclaimer">©Deakin University, School of Information Technology.
     This web page has been developed as a student assignment for the unit SIT203: Web Programming.
     Therefore it is not part of the University's authorised web site.
    DO NOT USE THE INFORMATION CONTAINED ON THIS WEB PAGE IN ANY WAY.
  </div>
   <div class="left_footer">
     <img src="images/footer_logo.gif" alt="" title="" /><br />
     <a href="http://csscreme.com/freecsstemplates/" title="free templates"><img src="images/csscreme.gif" alt="free templates" title="free templates" border="0" /></a>
   </div>
   <div class="right_footer">
     <a href="#">home</a>
     <a href="#">about us</a>
     <a href="#">flowers</a>
     <a href="#">privacy policy</a>
     <a href="#">contact us</a>
   </div>v
</div>


</div>
  <!-- sdelor js for xml functionality added to footer 10/9/2016-->
  <script type="text/javascript" src="js/xmlfunction.js"></script>
  <!-- sdelor script for main functions of forms etc 3/08/2016-->
  <script type="text/javascript" src="js/main.js"></script>
  
  <script type="text/javascript">
	$(function(){
		selectMenuItem("<?php echo basename($_SERVER['PHP_SELF']); ?>");
	});
  </script>
</body>
</html>

<!-- 
BIBILOGRAPHY

jquery
Code.jquery.com. (2016). jQuery. [online] Available at: https://code.jquery.com/jquery-3.1.0.min.js [Accessed 13 Aug. 2016].

Flowers Across Melbourne
Flowersacrossmelbourne.com.au. (2016). Flowers Across Melbourne. [online] Available at: https://www.flowersacrossmelbourne.com.au/ [Accessed 5 Aug. 2016].

loadXMLDOC(url)
Chapman, J. (2016). Object doesn't support property or method 'transformNode' in Internet Explorer 10 (Windows 8). [online] Stackoverflow.com. Available at: http://stackoverflow.com/questions/12149410/object-doesnt-support-property-or-method-transformnode-in-internet-explorer-1 [Accessed 14 Aug. 2016].
-->
