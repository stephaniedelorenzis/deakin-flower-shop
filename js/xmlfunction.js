function loadXMLDoc(url)
{
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else {
		xhr = new ActiveXObject("Microsoft.XMLHTTP"); // For IE 6
	}
	xhr.open("GET", url, false);
	try { xhr.responseType = "msxml-document"; } catch (e) {
		console.log(e)
	};
	xhr.send();
	return xhr.responseXML;
}

/*Loads all the products in the xmlR*/
function displayResult(xmlDoc, xslDoc, elementId)
{
   xml=loadXMLDoc(xmlDoc);
   xsl=loadXMLDoc(xslDoc);
  
  // code for IE
  if (window.ActiveXObject || xhr.responseType == "msxml-document")
  {
    ex=xml.transformNode(xsl);
    document.getElementById(elementId).innerHTML=ex;
  } 
  // code for Mozilla, Firefox, Opera, etc.
  else if (document.implementation && document.implementation.createDocument)
  {
     xsltProcessor=new XSLTProcessor();
     xsltProcessor.importStylesheet(xsl);
     resultDocument = xsltProcessor.transformToFragment(xml,document);
	 document.getElementById(elementId).appendChild(resultDocument);
  }
}


/*Function to load the flower details on the browse page*/
function displayFlowerDetails(xmlDoc, xslDoc, flowerId, elementId){
	
	var xml=loadXMLDoc(xmlDoc);
	var xsl=loadXMLDoc(xslDoc);

	// code for IE
	if (window.ActiveXObject || xhr.responseType=="msxml-document")
	{
		var template = new ActiveXObject('Msxml2.XslTemplate');
        template.stylesheet = xsl;
        var proc = template.createProcessor();
        proc.input = xml;
        proc.addParameter('flowerid', flowerId);
        proc.transform();
        document.getElementById(elementId).innerHTML = proc.output;
	}

	// code for Chrome, Firefox, Opera, etc.
	else if (document.implementation && document.implementation.createDocument)
	{
		 xsltProcessor = new XSLTProcessor();
		xsltProcessor.importStylesheet(xsl);

		xsltProcessor.setParameter(null, "flowerid", flowerId);

		resultDocument = xsltProcessor.transformToFragment(xml, document);
		document.getElementById(elementId).appendChild(resultDocument);
	}
}
