//WHEN USER CLICKS PAYPAL ALERT THAT IT ISN'T AVAILIABLE
//RETURNS CURSOR TO CREDIT CARD OPTION
function paypal()
{
	alert("Paypal is not availiable right now");
	$("#paymentCredit").prop("checked", true);
}

//HIGHLIGHT THE MENU ITEM
function selectMenuItem(pageName){
	//highlight the button that has the pageurl
	$("#menu li [href='"+pageName+"']").parent().addClass("selected");
}


/*
* Validate the control and return a valid status
*/
function validateControl(control) {
	var name = control.attr("name");
	var val = control.val();
		
	var valid;
	var msg = "";
		
	switch(name) {
		case "fname":
		case "lname":
		case "cardName":
		case "suburb":
		case "streetName":
			valid = validateName(val);
			msg = "Name is not valid";
			break;
		case "phone":
			valid = validatePhoneNumber(val);
			msg = "Phone number is not valid";
			break;
		case "cardNumber":
			valid =  validateCardNumber(val);
			msg = "Card number is no valid";
			break;
		case "cvc":
			valid = validateCvc(val);
			msg = "CVC is not valid";
			break;
		case "postcode":
			valid = validatePostcode(val);
			msg = "PostCode is not valid";
			break;
		case "email":
			valid = validateEmail(val);
			msg = "Email is not valid";
			break;
		case "expMonth":
		case "expYear":
		case "cardType":
		case "state":
			valid = (val != -1);
			msg = "Please select a choice";
			break;		
	}
	
	//return new object with valid status and a message to put in the alert box
	return {valid: valid, msg: msg};
}

/*
* Validates the name
*/
function validateName(name) {
	//letter, spaces, hyphens and apostrophes only
	return name.match(/^[a-z\- \']{1,}$/i);
}

/*
* Validate a phone number
*/
function validatePhoneNumber(number) {
	//can start with a + (optional)
	//numbers and spaces
	//8 - 11 chars
	return number.match(/^\+?[0-9 ]{8,11}$/);
}

/*
* Validate the email address
*/
function validateEmail(email) {
	//check for a-z any case
	//dots and hyphens and underscores in first part of email
	//one @ separating the parts
	//letters and dots in the second half of the email
	return email.match(/^[a-z\.\-_]*@[a-z\.]{1,}/i);
}

/*
* Validate the card number
*/
function validateCardNumber(number) {
	//4 sets of 4 numbers with an optional space separating them
	return number.match(/^([0-9]{4} ?){4}$/);
}

/*
* Validates the post code to 4 digits
*/
function validatePostcode(postcode) {
	//set of 4 numbers
	return postcode.match(/^[0-9]{4}$/);
}

/*
* Validate the CVC
*/
function validateCvc(cvc) {
	//set of three digits
	return cvc.match(/^[0-9]{3}$/)
}